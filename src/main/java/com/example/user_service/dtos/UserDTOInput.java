package com.example.user_service.dtos;
import org.springframework.web.multipart.MultipartFile;

public class UserDTOInput {
    private String name;
    private String age;
    private String email;
    private String password;
    private MultipartFile file;

    public UserDTOInput(String name, String age, String email, String password, MultipartFile file) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
