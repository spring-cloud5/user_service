package com.example.user_service.exception;

public class FileNotImageException extends RuntimeException {
    public FileNotImageException(String message) {
        super(message);
    }

    public FileNotImageException(String message, Throwable cause) {
        super(message, cause);
    }
}
