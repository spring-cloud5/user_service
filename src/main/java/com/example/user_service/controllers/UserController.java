package com.example.user_service.controllers;

import com.example.user_service.dtos.LoginDto;
import com.example.user_service.dtos.UserDTO;
import com.example.user_service.dtos.UserDTOInput;
import com.example.user_service.entites.User;
import com.example.user_service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/register")
    public User register(@RequestParam("name") String name , @RequestParam("age") String age , @RequestParam("password") String password , @RequestParam("email") String email , @RequestParam("file") MultipartFile file){
        return userService.register(name , age , password , email , file);
    }

//    @GetMapping(value = "/getByName/{name}")
//    public List<UserDTO> getUserByName(@PathVariable String name) {
//        return userService.getByName(name);
//    }

    @GetMapping(value = "/getById/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id){
        return userService.getById(id);
    }

    @PostMapping(value = "/login")
    public User login(@RequestBody LoginDto inputUser){
        return userService.login(inputUser.getName(),inputUser.getPassword());
    }


}
