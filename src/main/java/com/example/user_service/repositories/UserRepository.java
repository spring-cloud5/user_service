package com.example.user_service.repositories;
import com.example.user_service.entites.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;


public interface UserRepository extends JpaRepository<User, Long> {

    User findUserById(Long Id);
    User findUserByNameAndPassword(String Name,String Password);
    User findUserByName(String Name);
//    @Query("SELECT ud FROM User ud WHERE ud.name=?1")
//    List<User> findUserByName(String name);

    @Query("SELECT ud FROM User ud WHERE ud.name=?1")
    User findUserByUserName(String name);

    @Query("SELECT ud FROM User ud WHERE ud.password=?1 AND ud.name=?2")
    User findUserByPassword(String password , String name);


}
