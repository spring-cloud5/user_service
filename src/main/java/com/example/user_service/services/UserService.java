package com.example.user_service.services;

import com.example.user_service.dtos.UserDTO;
import com.example.user_service.dtos.UserDTOInput;
import com.example.user_service.entites.User;
import com.example.user_service.exception.FileNotImageException;
import com.example.user_service.exception.FileStorageException;
import com.example.user_service.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
//    private final Logger LOGGER = loggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository repository;

    public User register(String name, String age, String password, String email, MultipartFile file) {
        try {
            User user = new User(age, name, email, password, "", "");

            if (repository.findUserByUserName(name) != null) {
                System.out.println("test");
                return new User("User already exists");
            }
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            try {
                // Check if the file's name contains invalid characters
                if (fileName.contains("..")) {
                    throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
                }
                if (!file.getContentType().contains("image")) {
                    throw new FileNotImageException("Sorry! File Type Not image " + fileName + "  " + file.getContentType());
                }
                String image = new String(Base64.encodeBase64(file.getBytes()), "UTF-8");
                System.out.println("image : " + image);
                user.setImage(image);
                user.setImage_type(file.getContentType());
            } catch (IOException ex) {
                throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
            }

            System.out.println("User : " + name);
            try {
                User save = repository.save(user);
                System.out.println(" saveee : " + save.getEmail());
            } catch (Exception e) {
                System.out.println("Exception ");
                System.out.println(e);
            }
            return user;
            // return getUserDetailsDTO(user);
        } catch (Exception e) {
            System.out.println("Exception ");
            System.out.println(e);
        }
        return null;
    }

    public ResponseEntity<User> getById(Long id) {
        User user = repository.findUserById(id);
        if (user == null) {
            return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    public User login(String name, String password) {
        User user = repository.findUserByNameAndPassword(name, password);
        return user;
    }
//      User user2 = repository.findUserByPassword(password, name);
//
//
//        User userDTO = new User("not found");
//        boolean error1 = false;
//        boolean error2 = false;
//
//        if (user1 == null) {
//            error1 = true;
//        }
//
//        if (user2 == null) {
//            error2 = true;
//        }
//
//        if (error1 && error2) {
//            userDTO.setEmail("Error in userName and password");
//            return userDTO;
//        }
//        if (error1) {
//            userDTO.setEmail("Error in userName");
//            return userDTO;
//        }
//        if (error2) {
//            userDTO.setEmail("Error in password");
//            return userDTO;
//        }


//    public List<UserDTO> getByName(String name) {
//        List<User> userList = repository.findUserByName(name);
//        if (userList.isEmpty()) {
//            return null;
//        }
//        return userList.stream().map(this::getUserDetailsDTO).collect(Collectors.toList());
//    }

//    public UserDTO getUserDetailsDTO(User user) {
//        System.out.println("ibrajim " + user.getEmail());
//        return new UserDTO(
//                user.getId(),
//                user.getName(),
//                user.getAge(),
//                user.getEmail(),
//                user.getPassword(),
//                user.getImage(),
//                user.getImage_type()
//        );
//    }


}
